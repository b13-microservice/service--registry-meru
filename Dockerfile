FROM openjdk:8
EXPOSE 8761
ARG JAR_FILE=target/service-registry-meru.jar
WORKDIR /opt/app
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","app.jar"]