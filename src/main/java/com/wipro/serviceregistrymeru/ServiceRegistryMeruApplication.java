package com.wipro.serviceregistrymeru;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ServiceRegistryMeruApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceRegistryMeruApplication.class, args);
	}

}
